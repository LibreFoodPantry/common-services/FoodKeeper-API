package com.gitlab.LibreFoodPantry.FoodKeeperAPI.Models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Class representing a shelf life found in product data")
public class ShelfLife {	
	@ApiModelProperty(notes = "Minimum time for shelf life", example = "0", position = 0)
	private Integer min;
	@ApiModelProperty(notes = "Maximum time for shelf life", example = "6", position = 1)
	private Integer max;
	@ApiModelProperty(notes = "Metric for shelf life", example = "months", position = 2)
	private String metric;
	@ApiModelProperty(notes = "Storage tips", example = "Sealed", position = 3)
	private String tips;
	
	public ShelfLife() {}
	
	public ShelfLife(Integer min, Integer max, String metric, String tips) {
		this.min = min;
		this.max = max;
		this.metric = metric;
		this.tips = tips;
	}
	
	public Integer getMin() {
		return min;
	}
	
	public void setMin(Integer min) {
		this.min = min;
	}
	
	public Integer getMax() {
		return max;
	}
	
	public void setMax(Integer max) {
		this.max = max;
	}
	
	public String getMetric() {
		return metric;
	}
	
	public void setMetric(String metric) {
		this.metric = metric;
	}
	
	public String getTips() {
		return tips;
	}
	
	public void setTips(String tips) {
		this.tips = tips;
	}

	@Override
	public String toString() {
		return "ShelfLife [min=" + min + ", max=" + max + ", metric=" + metric + ", tips=" + tips;
	}
}
